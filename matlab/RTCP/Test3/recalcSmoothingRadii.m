clear
%clf reset

[DG,hdr]=readgadget('Test3_HiRes.dat');

r=DG.r/hdr.BoxSize;

Nsph=32;
[h,dn]=calcdens_d(r,Nsph,mean(DG.h)/hdr.BoxSize/2);

%[No,Xo]=hist(DG.h,[0:0.001:0.25]);
% Factor of 2 because Gadget is compact over h, while calcdens's h is over 2h
%[Nn,Xn]=hist(2*h*hdr.BoxSize,[0:0.001:0.25]);


%figure(1)
%plot(Xo,No,'k-',Xn,Nn,'r-')

% Copy the IC file Data Structure
DO=DG;
DO.h=2*hdr.BoxSize*h;
% Not sure the following does what we want. dn is a list of mass-weighted
% densities. Clumping will lead to particles having mean densities higher than
% the density of the clump.  <h>/<h_o> = 1, but <rho>/<rho_o> = 1.35
DO.rho=(hdr.mass(1)/hdr.BoxSize^3)*dn;
%DO.rho=DG.rho;

% IC File
disp(['mean(h/h_orig)=',num2str(mean(DO.h./DG.h))])
disp(['mean(rho/rho_orig)=',num2str(mean(DO.rho./DG.rho))])
fname='Test3_HiRes_Fixed_Nsph.dat';
writegadget(fname,DO,hdr)
