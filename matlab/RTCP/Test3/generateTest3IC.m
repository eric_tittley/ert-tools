clear
more off

unpertFileLo='/home/etittley/Num/Gadget/ICs/unpert/Glass-0032.data';
unpertFileHi='/home/etittley/Num/Gadget/ICs/unpert/Glass-0064.data';

Nsph=32;
fname='Test3_LoRes.dat';

% Load background glass
[Dg,hdrg]=readgadget(unpertFileLo,1);

n=length(Dg.id);

% Use the glass as the background.
Dg.r=Dg.r/hdrg.BoxSize; % normalize first
D=Dg;
hdr=hdrg;

% Update hdr
hdr.BoxSize=6.6; % kpc
hdr.HubbleParam=1.0; % kpc
hdr.redshift=0;

D.r=D.r*hdr.BoxSize; % kpc

% Load clump glass
[Dg,hdrg]=readgadget(unpertFileHi,1);

% Normalized radius to extract
clumpRadius=0.8/6.6; % normalized clump radius
clumpExtractRadius =  (199/8)^(1/3) * clumpRadius;

Dg.r=Dg.r/hdrg.BoxSize; % normalize radii

rToSample=Dg.r-0.5;
dist=sqrt(sum(rToSample.^2));
rClump=rToSample(:,dist<clumpExtractRadius);
% Shrink to the correct size
rClump=rClump*(clumpRadius/clumpExtractRadius);
clumpCentre=[5/6.6,0.5,0.5]; % Centre of the clump in normalized units
rClump(1,:)=rClump(1,:)+clumpCentre(1);
rClump(2,:)=rClump(2,:)+clumpCentre(2);
rClump(3,:)=rClump(3,:)+clumpCentre(3);
rClump=rClump*hdr.BoxSize;
D.r=[D.r,rClump];

% Update hdr
Npart=length(D.r);
hdr.npart=[Npart,0,0,0,0,0]';
hdr.npartTotal=hdr.npart;

% Mass per particle
% Total mass in volume 
SI='MKS'; hdr.h100=1; Units
Hmass = 1.00794*amu; % kg

% Gadget Internal Units
UnitMass=1e10*Mo;

n_lo = 2e2; % m^-3
n_hi = 4e4; % m^-3

rho_lo = Hmass*n_lo; % kg/m^3
rho_hi = Hmass*n_hi; % kg/m^3

totalMass =   rho_lo*(6.6*kpc)^3 ...
            + (rho_hi-rho_lo)*(4/3)*pi*(0.8*kpc)^3; % kg
totalMass = totalMass/UnitMass; % 10^10 Mo
massPerParticle = totalMass/Npart; % 10^10 Mo
hdr.mass=[massPerParticle 0 0 0 0 0]';

% Fill in u, rho, & h
% First, find the clump and non-clump particles
r=recentre(D.r/hdr.BoxSize,[5/6.6,0.5,0.5])-0.5;
dist=sqrt(sum(r.^2));
Clump=find(dist<clumpRadius);
Medium=find(dist>=clumpRadius);

% Smoothing Radius
hlo=(Nsph/n       * 3/(4*pi))^(1/3) * hdr.BoxSize;
hhi=(Nsph/(200*n) * 3/(4*pi))^(1/3) * hdr.BoxSize;
D.h=zeros(Npart,1);
D.h(Clump)=hhi;
D.h(Medium)=hlo;

% Density
UnitLength=kpc;
GadgetDensityUnit = UnitMass/UnitLength^3; % kg/m^3
rho_lo_GadgetUnit = rho_lo/GadgetDensityUnit;
rho_hi_GadgetUnit = rho_hi/GadgetDensityUnit;
D.rho         = zeros(Npart,1);
D.rho(Clump)  = rho_hi_GadgetUnit;
D.rho(Medium) = rho_lo_GadgetUnit;

% u will be filled in by Taranis/Manitou. But use this field as a tag for QC
D.u=zeros(Npart,1);
D.u(Clump)=40; 
D.u(Medium)=8000;

% Less important fields
% v
D.v=zeros(Npart,3);
% mass
D.mass=zeros(Npart,1)+hdr.mass(1);
% ID
D.id=[1:Npart];

% IC File
writegadget(fname,D,hdr)
