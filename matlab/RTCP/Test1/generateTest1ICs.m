clear
more off

% For 256^3
%InputGlassFile = 'Glass-0256.data';
%OutputFileRoot = 'gadget_glass_256_';

% For 128^3
%InputGlassFile = '/disc1/etittley/Num/Gadget2/ICs/unpert/Glass-0128.data';
%OutputFileRoot = 'gadget_glass_128_';

% For 64^3
InputGlassFile = '/disc1/etittley/Num/Gadget2/ICs/unpert/Glass-0064.data';
OutputFileRoot = 'gadget_glass_064_';

Nsph = 32;
Ls    = [6.6 13.2]; % kpc
nH   = 1e3; % m^-3
T    = 1e4; % K
Y    = 0; % Helium mass fraction

[D,H]=readgadget(InputGlassFile);

H.HubbleParam = 1.0;
H.redshift    = 0.0;

SI='MKS'; hdr.h100=H.HubbleParam; Units

kpc = Mpc/1000; % m

mH = 1.00794 * amu; % kg

nParticles=length(D.r);

MeanDensity = nH * mH; % kg m^-3

Loriginal = 1000; % kpc

UnitMass = 1e10*Mo/H.HubbleParam; % kg
UnitLength =  kpc / H.HubbleParam / (1.0+H.redshift); % m
mu = 4.0/(4.0-3.0*Y);
GadgetUtoTFactor = 1.e6 * (2./3.)*mu*mH/kb; % K
GadgetDensityUnit = UnitMass/UnitLength^3 ; % kg/m^3

D.r = D.r/Loriginal; % [0,1)

[h,dn]=calcdens_d(D.r,Nsph,0.001);

H.npart = [nParticles 0 0 0 0 0 ];
H.npartTotal = H.npart;

for iBoxSize = 1:2
 L = Ls(iBoxSize); % kpc
 Volume = (L*kpc)^3; % m^3
 TotalMassInVolume = MeanDensity * Volume; % kg
 MassPerParticle = TotalMassInVolume/nParticles; % kg

 D.r    = D.r*L;   % kpc
 D.h    = h*(2*L); % kpc (Gadget SPH kernel is compact over h, not 2h)
 D.v    = 0*D.r;
 D.id   = [1:nParticles];
 D.mass = ones(nParticles,1)*(MassPerParticle/UnitMass); % 1e10 Mo
 D.u    = ones(nParticles,1)*(T/GadgetUtoTFactor);
 D.rho  = ones(nParticles,1)*(MeanDensity/GadgetDensityUnit);

 H.mass  = [D.mass(1)  0 0 0 0 0 ];
 H.BoxSize = L;

 OutputFile = [OutputFileRoot,num2str(L),'kpc']
 writegadget(OutputFile,D,H)

 D.r    = D.r/L; % Reset
end
