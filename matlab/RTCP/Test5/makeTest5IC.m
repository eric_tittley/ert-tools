clear

SI='CGS'; hdr.h100=1; Units

GadgetUnitLength=3.085678e21;
GadgetUnitMass=1.989e43;
GadgetDensityUnit = GadgetUnitMass/GadgetUnitLength^3;
nTarget=1e-3;
mass_p = 1.0073*amu;
mH=mass_p+m_e;
rhoTarget=nTarget*mH;

[D,H]=readgadget('../Test1/gadget_glass_L13.2_N128_001');
H.BoxSize=30;

TotalMass=(H.BoxSize*kpc)^3 * rhoTarget;
MassPerParticle=TotalMass/H.npart(1) / GadgetUnitMass;

GadgetUtoTFactor = 1.e6 * (2./3.)*mass_p/kb
D.r=D.r.*(H.BoxSize/13.2);
D.v=D.v*0;
D.mass=D.mass*0+MassPerParticle;
D.u = D.u*0 + 100/GadgetUtoTFactor;
D.rho = D.rho*0+rhoTarget/GadgetDensityUnit;
Nsph = 32;
Hcalc = (Nsph*H.BoxSize^3/(4/3*pi*H.npart(1)))^(1/3)
D.h=D.h.*(H.BoxSize/13.2);

writegadget('Test5.gadget',D,H)
 
