clear
clf
L   = 1.600; % kpc
[D,H]=readgadget('Test6-320.gadget');
A=h_proj_3d(D.r,D.mass,D.h,512,L*[0 1 0 1 0 1]);
semilogy(A(:,256,256))
hold on
% for 147 (known to work)
% 20e-8 for 256, 320, & 400
plot([1:512],20.0e-8./([[256:-1:1],[1:256]].^2),'g');
set(gca,'YLim',5e-10.*[0 1]);
hold off

% verify the normalization
r_o = 91.5; % pc
n_o = 3.2; % cm^-3
SI='CGS'; hdr.h100=1; Units
GadgetUnitMass=1.989e43; % g
TargetClumpVolume=(4/3)*pi*(r_o*kpc/1000)^3;
mass_p = 1.0073*amu;
mH=mass_p+m_e;
TargetClumpDensity=n_o * mH;

TargetClumpMass=TargetClumpVolume*TargetClumpDensity % g

r=D.r-0.5*H.BoxSize; % Shift the centre to the origin [kpc]
distance = sqrt(sum(r.^2)); % kpc
iCore=find(distance<(r_o/1000));
ICClumpMass=sum(D.mass(iCore))*GadgetUnitMass %g
