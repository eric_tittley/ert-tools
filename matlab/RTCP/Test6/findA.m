function A=findA()
f=inline('abs(x./(1-x).^3 - 91.5^2)','x','X');
A=BracketMin(f,0.94,0.95,0.96,[]);

