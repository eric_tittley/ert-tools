clear

L   = 1600; % pc
r_o = 91.5; % pc
n_o = 3.2;  % cm^-3
T_o = 1000; % K

% Load a glass
GlassFile='/disc1/etittley/Num/Gadget/ICs/unpert/Glass-0800.data';
[DG,HG]=readgadget(GlassFile);
% Scale r to the boxsize, L
DG.r = DG.r.*(L/HG.BoxSize); % pc

% Cut a hole with radius r_o
r=DG.r-0.5*L; % Shift the centre to the origin [pc]
distance = sqrt(sum(r.^2)); % pc
iCore=find(distance<r_o);
rT = r(:,iCore); % The hole, now in the test r's list. [pc]

% Squeeze the remains linearly r' = r - Ar (0<A<1)
A=findA();
FudgeFactor=0.7194;
rp  = r.*(FudgeFactor*(1-A)); % pc

% Expand these by r'' = r' + Br'^3 (B>0 and has units of r^-2 )
% Take B = 1/pc^2
distp2 = sum(rp.^2); % pc^2
rpp = zeros(3,length(rp));
rpp(1,:) = rp(1,:).*(1+distp2);
rpp(2,:) = rp(2,:).*(1+distp2);
rpp(3,:) = rp(3,:).*(1+distp2);

% Isolate the halo
distance = sqrt(sum(rpp.^2)); % pc
iHalo=find(distance>=r_o);

% Add the core and the halos together
rT = [rT, rpp(:,iHalo)];

% Shift back to volume centre and trim off the excess particles.
rT = rT + 0.5*L;
good=find( rT(1,:)>0 & rT(1,:)<L & rT(2,:)>0 & rT(2,:)<L & rT(3,:)>0 & rT(3,:)<L );
rT = rT(:,good);

% Calculate mass per particle
SI='CGS'; hdr.h100=1; Units

N=length(rT);
GadgetUnitLength=3.085678e21;
GadgetUnitMass=1.989e43;
mass_p = 1.0073*amu;
mH=mass_p+m_e;
rhoTarget=n_o*mH;
TotalMass=(L/1000*kpc)^3 * rhoTarget;
MassPerParticle=TotalMass/length(DG.r) / GadgetUnitMass;
GadgetUtoTFactor = 1.e6 * (2./3.)*mass_p/kb;

GadgetDensityUnit = GadgetUnitMass / kpc^3; % Not positive about this.

% Initial smoothing length
r=rT ./ L; % [0, 1)
Nsph = 32;
[h,dn]=calcdens_d(r,Nsph,0.01);
% dn is number of particles / volume
dn = dn * MassPerParticle*GadgetUnitMass / (L/1000*kpc)^3 ; % g/cm^3

% Compose the output structures
%  Header
H             = HG; % Set defaults
H.npart(1)    = N;
H.npart(2)    = 0;
H.redshift    = 0;
H.npartTotal  = H.npart;
H.BoxSize     = L/1000; %kpc
H.HubbleParam = 1;
%  Data
D.r    = rem(rT./1000,L/1000); % kpc
D.v    = zeros(3,N);
D.id   = [1:N];
D.mass = zeros(1,N) + MassPerParticle;
D.u    = zeros(1,N) + T_o/GadgetUtoTFactor;
D.rho  = dn./GadgetDensityUnit;
D.h    = h.*(2*L/1000); % kpc [extra 2 because calcdens is compact over 2h]

% Write it
writegadget('Test6.gadget',D,H)
