# Edinburgh Radiative Transfer tools #

Tools for analyzing and testing the Taranis and Manitou Radiative Transfer libraries, developed at the University of Edinburgh for the implementation of ionizing radiative transfer in cosmological simulations.

### Current toolset languages ###

* Python
* Matlab/Octave
* Please add code of a specific language in a separate directory

### How do I get set up? ###

* python requirements
* matlab requirements

### Who do I talk to? ###

* Administered by Eric Tittley
* Contributors
 * Eric Tittley
 * Gennaro Di Pietro
 * Peter Bell
 